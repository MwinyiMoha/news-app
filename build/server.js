var express = require('express');
var open = require('open');
var path = require('path');

const app = express();
const port = 3000;

app.get('/', (req, res)=>{
    res.sendFile(path.join(__dirname, '../public/index.html'));
});

app.listen(port, (error)=>{
    if(error){
        console.log('Error: ', error);
    }else{
        open('localhost:'+port);
    }
});
