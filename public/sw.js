// Service Worker

self.addEventListener('fetch', (event)=>{
    console.log('Fetch ', event.request.url);
})

self.addEventListener('install', (event)=>{
    console.log('Installing...');
});

self.addEventListener('activate', (event)=>{
    console.log('Activating...');
});
